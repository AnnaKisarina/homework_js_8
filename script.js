const ADD = 'add';
const REMOVE = 'remove';

let colorP = document.getElementsByTagName('p');
console.log(colorP);

for (let i = 0; i < colorP.length; i++) {
    colorP[i].style.backgroundColor = '#ff0000';
}

let list = document.getElementById('optionsList');
console.log(list);
console.log(list.parentElement);

/*  variant 1  */
console.log(list.children, typeof list.children);

/*  variant 2  */
console.log(list.children[0], typeof list.children[0]);
console.log(list.children[1], typeof list.children[1]);
console.log(list.children[2], typeof list.children[2]);
console.log(list.children[3], typeof list.children[3]);

let changeP = document.getElementById('testParagraph');
changeP.innerHTML = 'This is a paragraph';
let addClass = document.getElementsByClassName('main-header');
console.log('#####');

iteratorFn(addClass, ADD);

function iteratorFn(elem, action)
{
    for (const iterator of elem) {
        if (action === ADD) {
            console.log(iterator);
            iterator.classList.add('nav-item');

            if (iterator.hasChildNodes()) {
                iteratorFn(iterator.children, action);
            }
        } else if (action === REMOVE) {
            iterator.classList.remove('section-title');
        }
    }
}

console.log('#####');

let removeClass = document.querySelectorAll('h2.section-title');
console.log(removeClass);
iteratorFn(removeClass, REMOVE);

// for test
let removeClassCheck = document.querySelectorAll('h2.section-title');
console.log(removeClassCheck);
